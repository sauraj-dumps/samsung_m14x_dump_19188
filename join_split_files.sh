#!/bin/bash

cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat system/system/system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system/system/system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system/system/system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system/system/priv-app/DualOutFocusViewer_S/DualOutFocusViewer_S.apk.* 2>/dev/null >> system/system/priv-app/DualOutFocusViewer_S/DualOutFocusViewer_S.apk
rm -f system/system/priv-app/DualOutFocusViewer_S/DualOutFocusViewer_S.apk.* 2>/dev/null
cat system/system/priv-app/GalaxyApps_OPEN/GalaxyApps_OPEN.apk.* 2>/dev/null >> system/system/priv-app/GalaxyApps_OPEN/GalaxyApps_OPEN.apk
rm -f system/system/priv-app/GalaxyApps_OPEN/GalaxyApps_OPEN.apk.* 2>/dev/null
cat system/system/priv-app/OneDrive_Samsung_v3/OneDrive_Samsung_v3.apk.* 2>/dev/null >> system/system/priv-app/OneDrive_Samsung_v3/OneDrive_Samsung_v3.apk
rm -f system/system/priv-app/OneDrive_Samsung_v3/OneDrive_Samsung_v3.apk.* 2>/dev/null
cat system/system/priv-app/SecSettings/SecSettings.apk.* 2>/dev/null >> system/system/priv-app/SecSettings/SecSettings.apk
rm -f system/system/priv-app/SecSettings/SecSettings.apk.* 2>/dev/null
cat system/system/priv-app/AREmoji/AREmoji.apk.* 2>/dev/null >> system/system/priv-app/AREmoji/AREmoji.apk
rm -f system/system/priv-app/AREmoji/AREmoji.apk.* 2>/dev/null
cat system/system/priv-app/AREmojiEditor/AREmojiEditor.apk.* 2>/dev/null >> system/system/priv-app/AREmojiEditor/AREmojiEditor.apk
rm -f system/system/priv-app/AREmojiEditor/AREmojiEditor.apk.* 2>/dev/null
cat system/system/app/HoneyBoard/HoneyBoard.apk.* 2>/dev/null >> system/system/app/HoneyBoard/HoneyBoard.apk
rm -f system/system/app/HoneyBoard/HoneyBoard.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat bootRE/boot.elf.* 2>/dev/null >> bootRE/boot.elf
rm -f bootRE/boot.elf.* 2>/dev/null
